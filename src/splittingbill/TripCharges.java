package splittingbill;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class TripCharges {

	private List<ParticipantCharges> charges = new ArrayList<ParticipantCharges>();
	
	public int getNumberOfParticipants() {
		return charges.size();
	}

	public List<ParticipantCharges> getCharges() {
		return charges;
	}
	
	public void calculateparticipantOwingMoney() {
		
		BigDecimal totalCharge = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);
		
		// total trip charge
		for (ParticipantCharges participantCHarges : charges) {
			totalCharge = totalCharge.add(participantCHarges.getParticipantTotalCharge());
		}
		
		// trip average charge per participant
		BigDecimal average = totalCharge.divide(new BigDecimal(getNumberOfParticipants()), 2, RoundingMode.HALF_UP);
		
		// per participant owing money
		for (ParticipantCharges participantCHarges : charges) {
			BigDecimal owing = average.subtract(participantCHarges.getParticipantTotalCharge());
			participantCHarges.setOwingMoney(owing);
		}
	}
}
