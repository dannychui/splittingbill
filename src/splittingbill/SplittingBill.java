package splittingbill;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;;

/**
 * 
 * @author danny.hui
 *
 */
public class SplittingBill {

	public static void main(String[] args) {
		
		SplittingBill splittingBill = new SplittingBill();
		
		try {
			String filename = splittingBill.validateCommandLineParameters(args);
			splittingBill.deleteOutputFile(filename + ".out");
			AllTripCharges allTripCharges = splittingBill.parseInputFile(filename);
			splittingBill.calculateAllTripOwingMoney(allTripCharges);
			splittingBill.outputAllOwingMoney(allTripCharges, filename + ".out");
		} catch (SplittingBillException sbe) {
			// already handled. 
		} catch (Throwable t) {
			System.out.println("SplittingBill entercounters a unexpected error.");
			t.printStackTrace();
		}
	}
	
	private void deleteOutputFile(String filename) {
		
		File output = new File(filename);
		output.delete();
	}

	private String validateCommandLineParameters(String[] args) {
		
		String filename = null;
		
		if (args.length == 1) {
			filename = args[0];
		} else if (args.length == 0) {
			usage();
			throw new SplittingBillException("SB-1", "Input filename not provided.");
		} else {
			System.out.println("Warning: extra commandline parameters are ignored.");
			filename = args[0];
		}
		
		return filename;
	}
	
	private void usage() {
		System.out.println("Usage: java com.cority.splittingbill.SplittingBill inputFilename");
	}

	public AllTripCharges parseInputFile(String filename) {
		
		AllTripCharges allTripCharges = new AllTripCharges();
	    int previousLineNum = 0;
	    int currentLineNum = 0;
	    
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			TripCharges tripCharges = new TripCharges();
			
		    while ((currentLineNum = readNextTrip(br, tripCharges, previousLineNum)) > (previousLineNum + 1)) {
		    	previousLineNum = currentLineNum;
		    	allTripCharges.getAllCharges().add(tripCharges);
		    	tripCharges = new TripCharges();
		    }
		    
		    br.close();
		    
		} catch (FileNotFoundException fnfe) {
			handleError("SB-2", "Input file is not found: " + filename);
		} catch (IOException ioe) {
			handleError("SB-3", "Not able to read input file: " + filename);
		} catch (NumberFormatException nfe) {
			handleError("SB-4", "A number is expected at Line " + (previousLineNum + 1));
		}
		
		return allTripCharges;
	}

	private int readNextTrip(BufferedReader br, TripCharges tripCharges, int previousLineNum) throws NumberFormatException, IOException {
		
		String line = null;
		int participantNum = 0;
	    int currentLineNum = previousLineNum;
	    
	    if ((line = br.readLine()) != null) {
	    	participantNum = Integer.parseInt(line);
	    	currentLineNum = ++previousLineNum;
	    	
	    	if (participantNum > 0 ) {
	    		
	    		for (int i = 0; i < participantNum; i++) {
	    			ParticipantCharges charges = new ParticipantCharges();
	    			currentLineNum = readNextParticipantCharges(br, charges, previousLineNum);
	    			
	    			// Here currentLineNum must be larger then previousLineNum. Otherwise, exception has been thrown already.
    				previousLineNum = currentLineNum;
    				tripCharges.getCharges().add(charges);
	    		}
	    	} else if (participantNum == 0) {
	    		// end of trips
	    		// handle the case where the input file has only one line with number zero.
	    		if (previousLineNum == 1) {
	    			handleError("SB-6", "The number of participants must be a positive number - Line " + previousLineNum);
	    		}
	    	} else {
	    		handleError("SB-6", "The number of participants must be a positive number - Line " + previousLineNum);
	    	}
	    } else if (previousLineNum > 0) {
	    	handleError("SB-11", "The end of trips should be followed by 0 in its own line - Line " + (previousLineNum + 1));
	    } else {
	    	//empty input file
	    	handleError("SB-5", "The input file is empty.");
	    }
	    
	    return currentLineNum;
	}

	private int readNextParticipantCharges(BufferedReader br, ParticipantCharges charges, int previousLineNum) throws NumberFormatException, IOException {
		
		String line = null;
		int chargesNum = 0;
	    int currentLineNum = previousLineNum;
	    
	    if ((line = br.readLine()) != null) {
	    	chargesNum = Integer.parseInt(line);
	    	currentLineNum = ++previousLineNum;
	    	
	    	if (chargesNum > 0 ) {
	    		for (int i = 0; i < chargesNum; i++) {
	    			BigDecimal charge = readNextCharge(br, previousLineNum);
	    			currentLineNum = ++previousLineNum;

    				charges.getCharges().add(charge);
	    		}
	    	} else {
	    		handleError("SB-7", "The number of charges must be a positive number - Line " + currentLineNum);
	    	}
	    } else {
	    	handleError("SB-8", "The number of charges is expected at Line " + currentLineNum);
	    }
	    
	    return currentLineNum;
	}

	private BigDecimal readNextCharge(BufferedReader br, int previousLineNum) throws IOException {
		String line = null;
		BigDecimal charge = null;
		
		if ((line = br.readLine()) != null) {
			double amount = Double.parseDouble(line);
			
			if (amount > 0) {
				charge = new BigDecimal(amount).setScale(2, RoundingMode.HALF_UP);
			} else {
				handleError("SB-10", "A positive amount is expected at Line " + (previousLineNum + 1));
			}
		} else {
			handleError("SB-9", "A charge is expected at Line " + (previousLineNum + 1));
		}
		
		return charge;
	}

	private void handleError(String errCode, String errMsg) {
		System.out.println(errMsg);
		throw new SplittingBillException(errCode, errMsg);
	}
	
	public void calculateAllTripOwingMoney(AllTripCharges allTripCharges) {
		
		for (TripCharges tripCharges : allTripCharges.getAllCharges()) {
			tripCharges.calculateparticipantOwingMoney();
		}
	}
	
	public void outputAllOwingMoney(AllTripCharges allTripCharges, String filename) throws IOException {
		
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filename)));
		 
		for (TripCharges tripCharges : allTripCharges.getAllCharges()) {
			
			for (ParticipantCharges participantCharges : tripCharges.getCharges()) {
				
				double d = participantCharges.getOwingMoney().doubleValue();
				String formattedAmount = "$" + Math.abs(d);
				formattedAmount = d >= 0? formattedAmount : "(" + formattedAmount + ")";
				out.println(formattedAmount);
			}
			
			out.println();
		}
		
		out.close();
	}
}
