package splittingbill;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class ParticipantCharges {

	private List<BigDecimal> charges = new ArrayList<BigDecimal>();
	private BigDecimal owingMoney;

	public List<BigDecimal> getCharges() {
		return charges;
	}
	
	public BigDecimal getParticipantTotalCharge() {
		
		BigDecimal bd = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);
		
		for (BigDecimal charge : charges) {
			bd = bd.add(charge);
		}
		
		return bd;
	}

	public BigDecimal getOwingMoney() {
		return owingMoney;
	}

	public void setOwingMoney(BigDecimal owingMoney) {
		this.owingMoney = owingMoney;
	}
	
}
