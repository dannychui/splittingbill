package splittingbill;

import java.util.ArrayList;
import java.util.List;

public class AllTripCharges {
	
	private int numberOfCampingTrips;
	private List<TripCharges> allCharges = new ArrayList<TripCharges>();
	
	public int getNumberOfCampingTrips() {
		return numberOfCampingTrips;
	}
	
	public void setNumberOfCampingTrips(int numberOfCampingTrips) {
		this.numberOfCampingTrips = numberOfCampingTrips;
	}

	public List<TripCharges> getAllCharges() {
		return allCharges;
	}
}
