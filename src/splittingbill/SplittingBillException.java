package splittingbill;

public class SplittingBillException extends RuntimeException {

	private String errorCode;
	private String errorMsg;
	private int lineNum;
	
	public SplittingBillException(String errorCode, String errorMsg) {
		
		super();
		
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public int getLineNum() {
		return lineNum;
	}
}
